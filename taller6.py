from flask import Flask, render_template

app = Flask(__name__)

@app.route ('/')
def saludo():
    return 'Hola, Bienvenidos al Dicionario de Slang Panameño!'

@app.route ("/")
def index():
    titulo = "dicionario4"
    PALABRA = ["Bulto","Chaniao","Xopa","Chantin"]
    SIGNIFICADO = ["Persona que estorba","Bien vestido","Hola","Casa"]
    return render_template("index.html", titulo=titulo, PALABRA=PALABRA, SIGNIFICADO=SIGNIFICADO) 

if __name__=="__main__":
    app.run(debug=True)
